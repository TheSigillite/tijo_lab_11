package pl.edu.pwsztar.domain.chess;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KnightTest {
    private RulesOfGame knight = new RulesOfGame.Knight();

    @Tag("Knight")
    @ParameterizedTest
    @CsvSource({
            "4, 4, 2, 5",
            "2, 5, 3, 7",
            "4, 4, 5, 6",
    })
    void checkCorrectMoveForKnight(int xStart, int yStart, int xStop, int yStop){
        assertTrue(knight.isCorrectMove(xStart, yStart, xStop, yStop));
    }

    @ParameterizedTest
    @CsvSource({
            "4, 4, 4, 4",
            "4, 4, 3, 7",
    })
    void checkIncorrectMoveForKnight(int xStart, int yStart, int xStop, int yStop){
        assertFalse(knight.isCorrectMove(xStart, yStart, xStop, yStop));
    }
}
