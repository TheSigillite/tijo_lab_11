package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame king;
    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame rock;
    private RulesOfGame queen;
    private RulesOfGame pawn;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("Rock") RulesOfGame rock,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Pawn") RulesOfGame pawn) {
        this.bishop = bishop;
        this.knight = knight;
        this.rock = rock;
        this.queen = queen;
        this.king = king;
        this.pawn = pawn;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        boolean ismovelegal;
        int xStart = figureMoveDto.getStart().charAt(0);
        int yStart = figureMoveDto.getStart().charAt(2);
        int xEnd = figureMoveDto.getDestination().charAt(0);
        int yEnd = figureMoveDto.getDestination().charAt(2);
        switch (figureMoveDto.getType()){
            case BISHOP:
                ismovelegal = bishop.isCorrectMove(xStart,yStart,xEnd,yEnd);
                break;
            case KNIGHT:
                ismovelegal = knight.isCorrectMove(xStart,yStart,xEnd,yEnd);
                break;
            case ROCK:
                ismovelegal = rock.isCorrectMove(xStart,yStart,xEnd,yEnd);
                break;
            case QUEEN:
                ismovelegal = queen.isCorrectMove(xStart,yStart,xEnd,yEnd);
                break;
            case KING:
                ismovelegal = king.isCorrectMove(xStart,yStart,xEnd,yEnd);
                break;
            case PAWN:
                ismovelegal = pawn.isCorrectMove(xStart,yStart,xEnd,yEnd);
                break;
            default:
                ismovelegal = false;
        }
        return ismovelegal;
    }
}
